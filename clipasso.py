# sudo cog push r8.im/yael-vinker/clipasso

# Prediction interface for Cog ⚙️
# https://github.com/replicate/cog/blob/main/docs/python.md

import numpy as np
import os
import PIL
import pydiffvg
import sys
import torch
import traceback

from PIL import Image
from shutil import copyfile
from torchvision import models, transforms

# import subprocess as sp
# import imageio
# import re
# import matplotlib.pyplot as plt
# import multiprocessing as mp
# import math
# import argparse
# import time
# import torch.nn.functional as F
# import torch.nn as nn
# from tqdm import tqdm

import sketch_utils as utils
from config import Config
from models.loss import Loss
from models.painter_params import Painter, PainterOptimizer


class Clipasso():
    def __init__(
        self,
        num_iter=2001,
        num_sketches=3,
        use_gpu=True,
        num_strokes=3,
        mask_object=0,
        fix_scale=0,
        multiprocess=True
    ):
        self.num_iter = num_iter
        self.save_interval = 100
        self.num_sketches = num_sketches
        self.use_gpu = use_gpu
        self.mask_object = mask_object
        self.num_strokes = num_strokes
        self.fix_scale = fix_scale
        self.multiprocess = multiprocess

        if not torch.cuda.is_available():
            self.use_gpu = False

        self.losses_all = {}

        # TODO: yaml
        self.config = Config(
            num_paths=self.num_strokes,
            num_iter=self.num_iter,
            save_interval=self.save_interval,
            use_gpu=self.use_gpu,
            fix_scale=self.fix_scale,
            mask_object=self.mask_object,
            mask_object_attention=self.mask_object,
            num_sketches=self.num_sketches
        )

    def setup(self):
        pass

    def predict(self, image_path):
        target_image_name = os.path.basename(str(image_path))

        abs_path = os.path.abspath(os.getcwd())

        target = str(image_path)
        assert os.path.isfile(target), f"{target} does not exists!"

        test_name = os.path.splitext(target_image_name)[0]
        output_dir = f"{abs_path}/output_sketches/{test_name}/"
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        # TODO: remove
        self.config.target = target
        self.config.output_dir = output_dir

        for seed in list(range(0, self.num_sketches * 1000, 1000)):
            wandb_name = f"{test_name}_{self.num_strokes}strokes_seed{seed}"
            self.config.wandb_name = wandb_name  # TODO: remove

            args = self.config.call(seed)

            final_config = vars(args)  # TODO: wat?

            try:
                configs_to_save = main(args)
            except BaseException as err:
                print(f"Unexpected error occurred:\n {err}")
                print(traceback.format_exc())
                sys.exit(1)

            for k in configs_to_save.keys():
                final_config[k] = configs_to_save[k]

            # np.save(f"{self.config.output_dir}/config.npy", final_config)
            # config = np.load(f"{output_dir}/{wandb_name}/config.npy", allow_pickle=True)[()]

            loss_eval = np.array(final_config['loss_eval'])
            inds = np.argsort(loss_eval)
            self.losses_all[wandb_name] = loss_eval[inds][0]

        sorted_final = dict(
            sorted(self.losses_all.items(), key=lambda item: item[1])
        )

        copyfile(f"{output_dir}/{list(sorted_final.keys())[0]}/best_iter.svg",
                 f"{output_dir}/{list(sorted_final.keys())[0]}_best.svg")

        svg_files = os.listdir(output_dir)
        svg_files = [f for f in svg_files if "best.svg" in f]
        svg_output_path = f"{output_dir}/{svg_files[0]}"
        sketch_res = read_svg(svg_output_path, multiply=True).cpu().numpy()
        sketch_res = Image.fromarray((sketch_res * 255).astype('uint8'), 'RGB')
        sketch_res.save(f"{abs_path}/output_sketches/sketch.png")

        return svg_output_path


# ================================


# class Predictor(BasePredictor):
#     def setup(self):
#         """Load the model into memory to make running multiple predictions efficient"""
#         self.num_iter = 2001
#         self.save_interval = 100
#         self.num_sketches = 3
#         self.use_gpu = True

#     def predict(
#         self,
#         target_image: Path = Input(
#             description="Input image (square, without background)"),
#         num_strokes: int = Input(
#             description="The number of strokes used to create the sketch, which determines the level of abstraction", default=16),
#         trials: int = Input(
#             description="It is recommended to use 3 trials to recieve the best sketch, but it might be slower", default=3),
#         mask_object: int = Input(
#             description="It is recommended to use images without a background, however, if your image contains a background, you can mask it out by using this flag with 1 as an argument", default=0),
#         fix_scale: int = Input(
#             description="If your image is not squared, it might be cut off, it is recommended to use this flag with 1 as input to automatically fix the scale without cutting the image", default=0),
#     ) -> Path:

#         self.num_sketches = trials
#         target_image_name = os.path.basename(str(target_image))

#         abs_path = os.path.abspath(os.getcwd())

#         target = str(target_image)
#         assert os.path.isfile(target), f"{target} does not exists!"

#         test_name = os.path.splitext(target_image_name)[0]
#         output_dir = f"{abs_path}/output_sketches/{test_name}/"
#         if not os.path.exists(output_dir):
#             os.makedirs(output_dir)

#         if not torch.cuda.is_available():
#             self.use_gpu = False

#         losses_all = {}

#         cfg = Config(
#             target=target,
#             num_paths=num_strokes,
#             output_dir=output_dir,
#             wandb_name=wandb_name,
#             num_iter=self.num_iter,
#             save_interval=self.save_interval,
#             seed=seed,
#             use_gpu=self.use_gpu,
#             fix_scale=fix_scale,
#             mask_object=mask_object,
#             mask_object_attention=mask_object,
#             display_logs=0
#         )

#         seeds = list(range(0, self.num_sketches * 1000, 1000))

#         for seed in seeds:
#             wandb_name = f"{test_name}_{num_strokes}strokes_seed{seed}"

#             args = cfg.call(seed)

#             final_config = vars(args)  # TODO: wat?

#             try:
#                 configs_to_save = main(args)
#             except BaseException as err:
#                 print(f"Unexpected error occurred:\n {err}")
#                 print(traceback.format_exc())
#                 sys.exit(1)

#             for k in configs_to_save.keys():
#                 final_config[k] = configs_to_save[k]
#             np.save(f"{args.output_dir}/config.npy", final_config)

#             config = np.load(f"{output_dir}/{wandb_name}/config.npy",
#                              allow_pickle=True)[()]
#             loss_eval = np.array(config['loss_eval'])
#             inds = np.argsort(loss_eval)
#             losses_all[wandb_name] = loss_eval[inds][0]
#             # return Path(f"{output_dir}/{wandb_name}/best_iter.svg")

#         sorted_final = dict(
#             sorted(losses_all.items(), key=lambda item: item[1])
#         )

#         copyfile(f"{output_dir}/{list(sorted_final.keys())[0]}/best_iter.svg",
#                  f"{output_dir}/{list(sorted_final.keys())[0]}_best.svg")

#         svg_files = os.listdir(output_dir)
#         svg_files = [f for f in svg_files if "best.svg" in f]
#         svg_output_path = f"{output_dir}/{svg_files[0]}"
#         sketch_res = read_svg(svg_output_path, multiply=True).cpu().numpy()
#         sketch_res = Image.fromarray((sketch_res * 255).astype('uint8'), 'RGB')
#         sketch_res.save(f"{abs_path}/output_sketches/sketch.png")
#         return Path(svg_output_path)


def load_renderer(args, target_im=None, mask=None):
    renderer = Painter(
        num_strokes=args.num_paths,
        args=args,
        num_segments=args.num_segments,
        imsize=args.image_scale,
        device=args.device,
        target_im=target_im,
        mask=mask
    )
    return renderer.to(args.device)


def get_image(path: str):
    image = Image.open(path)

    if image.mode == "RGBA":
        # Create a white rgba background
        new_image = Image.new("RGBA", image.size, "WHITE")
        # Paste the image on the background.
        new_image.paste(image, (0, 0), image)
        image = new_image

    return image.convert("RGB")


def get_target(args: Config):
    image = get_image(args.target)

    masked_im, mask = utils.get_mask_u2net(args, image)

    if args.mask_object:
        image = masked_im
    if args.fix_scale:
        image = utils.fix_image_scale(image)

    transformed_image = transform_image(image, args.image_scale, args.device)

    return transformed_image, mask


def transform_image(image, scale, device):
    _transforms = []
    if image.size[0] != image.size[1]:
        _transforms.append(
            transforms.Resize(
                (scale, scale),
                interpolation=PIL.Image.BICUBIC
            )
        )
    else:
        _transforms.append(
            transforms.Resize(
                scale,
                interpolation=PIL.Image.BICUBIC
            )
        )
        _transforms.append(
            transforms.CenterCrop(scale)
        )
    _transforms.append(transforms.ToTensor())

    data_transforms = transforms.Compose(_transforms)

    return data_transforms(image).unsqueeze(0).to(device)


def main(args: Config):
    loss_func = Loss(args)
    inputs, mask = get_target(args)

    # utils.log_input(inputs, args.output_dir) original image

    renderer = load_renderer(args, inputs, mask)

    optimizer = PainterOptimizer(args, renderer)
    configs_to_save = {"loss_eval": []}
    best_loss, best_fc_loss = 100, 100
    best_iter = 0
    min_delta = 1e-5
    terminate = False

    renderer.set_random_noise(0)
    renderer.init_image(stage=0)

    optimizer.init_optimizers()

    epoch_range = enumerate(range(args.num_iter))

    for epoch, index in epoch_range:
        renderer.set_random_noise(epoch)

        if args.lr_scheduler:
            optimizer.update_lr(index)

        optimizer.zero_grad_()
        sketches = renderer.get_image().to(args.device)
        losses_dict = loss_func(
            sketches,
            inputs.detach(),
            renderer.get_color_parameters(),
            renderer,
            index,
            optimizer
        )

        loss = sum(list(losses_dict.values()))
        loss.backward()
        optimizer.step_()

        if epoch % args.eval_interval == 0:
            with torch.no_grad():
                losses_dict_eval = loss_func(
                    sketches,
                    inputs,
                    renderer.get_color_parameters(),
                    renderer.get_points_parans(),
                    index,
                    optimizer,
                    mode="eval"
                )
                loss_eval = sum(list(losses_dict_eval.values()))

                configs_to_save["loss_eval"].append(loss_eval.item())

                for k in losses_dict_eval.keys():
                    if k not in configs_to_save.keys():
                        configs_to_save[k] = []
                    configs_to_save[k].append(losses_dict_eval[k].item())

                if args.clip_fc_loss_weight and losses_dict_eval["fc"].item() < best_fc_loss:
                    best_fc_loss = losses_dict_eval["fc"].item(
                    ) / args.clip_fc_loss_weight

                current_delta = loss_eval.item() - best_loss
                if abs(current_delta) > min_delta and current_delta < 0:
                    best_loss = loss_eval.item()
                    best_iter = epoch
                    terminate = False
                    utils.plot_batch(
                        inputs,
                        sketches,
                        args.output_dir,
                        title="best_iter.jpg"
                    )
                    renderer.save_svg(args.output_dir, "best_iter")

                if abs(current_delta) <= min_delta:
                    if terminate:
                        break
                    terminate = True

        # if args.debug:
        #     if epoch % args.save_interval == 0:
        #         utils.plot_batch(
        #             inputs,
        #             sketches,
        #             f"{args.output_dir}/jpg_logs",
        #             title=f"iter{epoch}.jpg"
        #         )
        #         renderer.save_svg(
        #             f"{args.output_dir}/svg_logs",
        #             f"svg_iter{epoch}"
        #         )
        #     if index == 0 and args.attention_init:
        #         utils.plot_atten(
        #             renderer.get_attn(),
        #             renderer.get_thresh(),
        #             inputs,
        #             renderer.get_inds(),
        #             "{}/{}.jpg".format(args.output_dir, "attention_map"),
        #             args.saliency_model
        #         )

    renderer.save_svg(args.output_dir, "final_svg")
    path_svg = os.path.join(args.output_dir, "best_iter.svg")

    utils.log_sketch_summary_final(
        path_svg, args.device, best_iter, best_loss, "best total"
    )

    return configs_to_save


def read_svg(path_svg, multiply=False):
    device = torch.device("cuda" if (
        torch.cuda.is_available() and torch.cuda.device_count() > 0) else "cpu")

    canvas_width, canvas_height, shapes, shape_groups = pydiffvg.svg_to_scene(
        path_svg)

    if multiply:
        canvas_width *= 2
        canvas_height *= 2
        for path in shapes:
            path.points *= 2
            path.stroke_width *= 2

    _render = pydiffvg.RenderFunction.apply
    scene_args = pydiffvg.RenderFunction.serialize_scene(
        canvas_width, canvas_height, shapes, shape_groups
    )
    img = _render(
        canvas_width,  # width
        canvas_height,  # height
        2,   # num_samples_x
        2,   # num_samples_y
        0,   # seed
        None,
        *scene_args
    )
    img = img[:, :, 3:4] * img[:, :, :3] + \
        torch.ones(img.shape[0], img.shape[1], 3,
                   device=device) * (1 - img[:, :, 3:4])
    return img[:, :, :3]

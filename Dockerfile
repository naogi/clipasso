FROM docker.io/nvidia/cuda:11.4.1-cudnn8-runtime-ubuntu20.04

WORKDIR /app

COPY requirements.txt /app/

RUN apt-get update -qq \
    && DEBIAN_FRONTEND=noninteractive apt-get install -yq build-essential gnupg2 curl \
    git ssh vim imagemagick ffmpeg \
    python3-dev python3-venv python3-setuptools \
    gcc g++ make cmake software-properties-common && \
    add-apt-repository ppa:deadsnakes/ppa && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y python3.7 python3.7-dev python3.7-distutils python3.7-venv

RUN python3.7 -m ensurepip --upgrade
RUN ln -s /usr/bin/python3.7 /usr/local/bin/python
RUN cat requirements.txt | xargs -n 1 -L 1 pip3 install --no-cache-dir

RUN apt install cuda-toolkit-11-7

RUN git clone https://github.com/yael-vinker/CLIPasso.git && \
    mv ./CLIPasso/* ./ && \
    git clone https://github.com/BachiLi/diffvg && \
    cd diffvg && \
    git submodule update --init --recursive && \
    CUDA_TOOLKIT_ROOT_DIR=/usr/local/cuda/ python3.7 setup.py install

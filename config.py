import os
import random
import numpy as np
import pydiffvg
import torch


class Config():
    def __init__(self,
                 target=None,
                 num_paths=16,
                 output_dir=None,
                 wandb_name=None,
                 num_iter=500,
                 save_interval=10,
                 use_gpu=False,
                 fix_scale=0,
                 mask_object=False,
                 mask_object_attention=False,
                 num_sketches=3):
        self.target = target
        self.output_dir = output_dir  # directory to save the output images and loss
        self.path_svg = 'none'  # if you want to load an svg file and train from it")
        self.use_gpu = use_gpu
        self.mask_object = mask_object
        self.fix_scale = fix_scale
        self.display_logs = False
        self.display = False
        self.use_wandb = False
        self.wandb_user = 'yael-vinker'
        self.wandb_name = wandb_name
        self.wandb_project_name = 'wandb_project_name'
        self.num_iter = num_iter  # number of optimization iterations",

        # training stages, you can train x strokes, then freeze them and train another x strokes etc.")
        self.num_stages = 1

        self.lr_scheduler = False
        self.lr = 1.0
        self.color_lr = 0.01
        self.color_vars_threshold = 0.0
        self.batch_size = 1  # for optimization it's only one image")
        self.save_interval = save_interval
        self.eval_interval = 10
        self.image_scale = 224
        self.num_paths = num_paths  # number of strokes"
        self.width = 1.5  # stroke width"
        self.control_points_per_seg = 4

        # number of segments for each stroke, each stroke is a bezier curve with 4 control points")
        self.num_segments = 1

        # if True, use the attention heads of Dino model to set the location of the initial strokes")
        self.attention_init = True
        self.saliency_model = "clip"
        self.saliency_clip_model = "ViT-B/32"
        self.xdog_intersec = 1
        self.mask_object_attention = mask_object_attention
        self.softmax_temp = 0.3
        # the type of perceptual loss to be used (L2/LPIPS/none)
        self.percep_loss = 'none'
        self.perceptual_weight = 0  # weight the perceptual loss
        self.train_with_clip = False
        self.clip_weight = 0
        self.start_clip = 0
        self.num_aug_clip = 4
        self.include_target_in_aug = 0  # TODO: remove

        # if you want to apply the affine augmentation to both the sketch and image")
        self.augment_both = 1

        # can be any combination of: 'affine_noise_eraserchunks_eraser_press'")
        self.augemntations = "affine"
        self.noise_thresh = 0.5
        self.aug_scale_min = 0.7
        # if True, use L1 regularization on stroke's opacity to encourage small number of strokes")
        self.force_sparse = False

        self.clip_conv_loss = 1
        self.clip_conv_loss_type = "L2"
        self.clip_conv_layer_weights = [0.0, 0.0, 1.0, 1.0, 0.0]
        self.clip_model_name = "RN101"
        self.clip_fc_loss_weight = 0.1
        self.clip_text_guide = 0
        self.text_target = 'none'

        self.num_sketches = num_sketches

    def call(self, seed=0):
        self.set_seed(seed)
        self.prepare_arguments()

        return self

    def set_seed(self, seed):
        random.seed(seed)
        np.random.seed(seed)
        os.environ['PYTHONHASHSEED'] = str(seed)
        torch.manual_seed(seed)
        torch.cuda.manual_seed(seed)
        torch.cuda.manual_seed_all(seed)

    def prepare_arguments(self):
        self.output_dir = os.path.join(self.output_dir, self.wandb_name)
        if not os.path.exists(self.output_dir):
            os.mkdir(self.output_dir)

        jpg_logs_dir = f"{self.output_dir}/jpg_logs"
        svg_logs_dir = f"{self.output_dir}/svg_logs"
        if not os.path.exists(jpg_logs_dir):
            os.mkdir(jpg_logs_dir)
        if not os.path.exists(svg_logs_dir):
            os.mkdir(svg_logs_dir)

        if (self.use_gpu and torch.cuda.is_available() and torch.cuda.device_count() > 0):
            self.device = torch.device("cuda")
            pydiffvg.set_use_gpu(True)
        else:
            self.device = torch.device("cpu")

        pydiffvg.set_device(self.device)
